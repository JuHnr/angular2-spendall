#  Spend All Bank

This project use Angular version 17.2.1.

## Create a new project

### Prerequisites

Install npm

Install Angular 
``` bash
npm install -g @angular/cli
``` 

### Initiate an Angular project

``` 
ng new projectName --skip-tests=true
``` 

This will create a new project folder with the name "projectName" in the current folder.

Tap Enter and answer the questions the CLI asked. 
You can select your style language (CSS, SCSS...) and select 'Yes' for SSR.

## Install an existing Angular project

Copy the SSH key and run : 

``` 
git clone 
``` 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

Ctrl + C to quit.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
